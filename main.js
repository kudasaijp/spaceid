async function check(domain) {
  const proxy = new URL('/proxy', location.origin);
  proxy.searchParams.set('name', domain);

  const res = await fetch(proxy);
  return await res.json();
}

document.querySelector('form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const submitButton = document.getElementById('submit-button');
  submitButton.disabled = true;
  submitButton.value = 'Checking...';

  const domains = e.target.elements.domains.value.trim().split(/\r\n|\n/);
  const results = await Promise.all(
    domains.map((domain) => check(domain.replace('.bnb', '')))
  );

  let html = '';
  for (const result of results) {
    const status = result.Owner ? '× reserved' : '○ available';
    const className = result.Owner ? 'reserved' : 'available';
    html += `<div class="${className}"><a href="https://app.space.id/name/${result.name}.bnb/register" target="_blank">${result.name}</a>: ${status}</div>`;
  }

  submitButton.disabled = false;
  submitButton.value = 'Check';

  const output = document.getElementById('output');
  output.innerHTML = html + output.innerHTML;
});

document.getElementById('hide-reserved').addEventListener('change', (e) => {
  const output = document.getElementById('output');
  output.classList.toggle('hide-reserved', e.target.checked);
});
