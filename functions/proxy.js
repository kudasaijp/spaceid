export async function onRequest({ request }) {
  const name = new URL(request.url).searchParams.get('name');

  const res = await fetch('https://backend.prd.space.id/nameof', {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      ChainId: 56,
      name,
    }),
  });

  return new Response(await res.text());
}
